// 1.Опишіть своїми словами, що таке метод об'єкту
//  Функція, яка здійснює дії з властивостями, методами обєкту. Також виконує будь-які дії що й і звичайна функція.

// 2.Який тип даних може мати значення властивості об'єкта?
// Number, String, Null, Undefined, Boolean,  BigInt, Symbol, Object.

// 3.Об'єкт це посилальний тип даних. Що означає це поняття?
//Це означає, що об'єкт передається по ссилці. Наприклад const newOdj = {}; const newObj2 = newObj; де у змінних зберігається ссилка на один і той же самий об'єкт.


//Завдання

let inputFirstName = document.querySelector('.input-first-name');
let inputLastName = document.querySelector('.input-last-name');

function createNewUser (firstName = 'title', lastName = 'title') {

    const newUser = {
       _firstName: firstName,
       _lastName: lastName,

       getLogin () {
           return (this._firstName.slice(0, 1) + this._lastName).toLowerCase();
       },
   };
return newUser;
}

document.querySelector(".button").addEventListener("click", () => {
    const user = createNewUser(inputFirstName.value, inputLastName.value)
    document.querySelector(".output").innerText = "Login: " + user.getLogin();
    console.log("Login: " + user.getLogin());
});


